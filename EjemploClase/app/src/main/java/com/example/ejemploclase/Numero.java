package com.example.ejemploclase;


public class Numero {
    private Double numero;

    public Double getNumero(){
        return this.numero;
    }

    public void setNumero (Double numero){
        this.numero = numero;
    }

    public Numero(){
        this.numero = 0.0;
    }
}
