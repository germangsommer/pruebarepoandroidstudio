package com.example.ejemploclase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText textoEntrada = (EditText) findViewById(R.id.textoEntrada);
        final TextView textoSalida = (TextView) findViewById(R.id.textoSalida);
        Button botonMas = (Button) findViewById(R.id.botonMas);
        Button botonMenos = (Button) findViewById(R.id.botonMenos);
        Button botonPor = (Button) findViewById(R.id.botonPor);
        Button botonBarra = (Button) findViewById(R.id.botonBarra);
        Button botonBorrar = (Button) findViewById(R.id.botonBorrar);
        Button botonIgual = (Button) findViewById(R.id.botonIgual);
        final Numero resParcial = new Numero();
        Numero numEntrada = new Numero();
        Numero salidaNumero = new Numero();
        final Signo ultimoSigno = new Signo();


        botonMas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(textoEntrada.getText().toString().equals("")){
                    textoSalida.setText(resParcial.getNumero().toString());
                    ultimoSigno.setValor("+");
                } else{
                    if(!textoEntrada.getText().toString().equals("")){
                            resParcial.setNumero(resParcial.getNumero() + Integer.valueOf(textoEntrada.getText().toString()));
                            textoSalida.setText(resParcial.getNumero().toString());
                            textoEntrada.setText("");
                            ultimoSigno.setValor("+");
                        }
                }
            }
        });
        botonMenos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(textoEntrada.getText().toString().equals("")){
                    textoSalida.setText(resParcial.getNumero().toString());
                    ultimoSigno.setValor("-");
                } else{
                    if(!textoEntrada.getText().toString().equals("")){
                        if(resParcial.getNumero().equals(0.0)) {
                            resParcial.setNumero(Double.valueOf(textoEntrada.getText().toString()));
                            textoSalida.setText(resParcial.getNumero().toString());
                            textoEntrada.setText("");
                            ultimoSigno.setValor("-");
                        } else {
                            if(!resParcial.getNumero().equals(0.0)){
                                resParcial.setNumero(resParcial.getNumero() - Integer.valueOf(textoEntrada.getText().toString()));
                                textoSalida.setText(resParcial.getNumero().toString());
                                textoEntrada.setText("");
                                ultimoSigno.setValor("-");
                            }
                        }

                    }
                }
            }
        });
        botonPor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(textoEntrada.getText().toString().equals("")){
                    textoSalida.setText(resParcial.getNumero().toString());
                    ultimoSigno.setValor("*");
                } else{
                    if(resParcial.getNumero().equals(0.0)) {
                        resParcial.setNumero(Double.valueOf(textoEntrada.getText().toString()));
                        textoSalida.setText(resParcial.getNumero().toString());
                        textoEntrada.setText("");
                        ultimoSigno.setValor("*");
                    } else {
                        if(!textoEntrada.getText().toString().equals("")){
                            resParcial.setNumero(resParcial.getNumero() * Integer.valueOf(textoEntrada.getText().toString()));
                            textoSalida.setText(resParcial.getNumero().toString());
                            textoEntrada.setText("");
                            ultimoSigno.setValor("*");
                        }
                    }
                }
            }
        });
        botonBarra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(textoEntrada.getText().toString().equals("")){
                    textoSalida.setText(resParcial.getNumero().toString());
                    ultimoSigno.setValor("/");
                } else{
                    if(resParcial.getNumero().equals(0.0)) {
                        resParcial.setNumero(Double.valueOf(textoEntrada.getText().toString()));
                        textoSalida.setText(resParcial.getNumero().toString());
                        textoEntrada.setText("");
                        ultimoSigno.setValor("/");
                    } else {
                        if(Integer.valueOf(textoEntrada.getText().toString()) == 0){
                            textoSalida.setText("Error, Division por 0");
                            textoEntrada.setText("");
                        } else {
                            if(!textoEntrada.getText().toString().equals("")){
                                resParcial.setNumero(resParcial.getNumero() / Integer.valueOf(textoEntrada.getText().toString()));
                                textoSalida.setText(resParcial.getNumero().toString());
                                textoEntrada.setText("");
                                ultimoSigno.setValor("/");
                            }
                        }
                    }
                }
            }
        });
        botonBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resParcial.setNumero(0.0);
                textoSalida.setText("0.0");
                textoEntrada.setText("");
                ultimoSigno.setValor("");
            }
        });
        ;
        botonIgual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ultimoSigno.getValor().equals("")){
                    resParcial.setNumero(Double.valueOf(textoEntrada.getText().toString()));
                }
                if(ultimoSigno.getValor().equals("+")){
                    resParcial.setNumero(resParcial.getNumero() + Integer.valueOf(textoEntrada.getText().toString()));
                    textoSalida.setText(resParcial.getNumero().toString());
                    textoEntrada.setText("");
                }
                else{
                    if(ultimoSigno.getValor().equals("-")){
                        resParcial.setNumero(resParcial.getNumero() - Integer.valueOf(textoEntrada.getText().toString()));
                        textoSalida.setText(resParcial.getNumero().toString());
                        textoEntrada.setText("");
                    }
                else{
                    if(ultimoSigno.getValor().equals("*")){
                        resParcial.setNumero(resParcial.getNumero() * Integer.valueOf(textoEntrada.getText().toString()));
                        textoSalida.setText(resParcial.getNumero().toString());
                        textoEntrada.setText("");
                    }
                else{
                    if(ultimoSigno.getValor().equals("/")){
                        if(Integer.valueOf(textoEntrada.getText().toString()) == 0) {
                            textoSalida.setText("Error, Division por 0");
                            textoEntrada.setText("");
                        }
                        else{
                            if(Integer.valueOf(textoEntrada.getText().toString()) != 0){
                                resParcial.setNumero(resParcial.getNumero() / Integer.valueOf(textoEntrada.getText().toString()));
                                textoSalida.setText(resParcial.getNumero().toString());
                                textoEntrada.setText("");
                            }
                        }
                    }
                }
            }
        };
    };
        });
}
}
